export default class Tag {
    constructor() {
        this.tagName = ''
        this.type = ''
        this.id = ''
        this.classNames = []
        this.children = []
        this.textNode = []
    }

    addChildren(tag) {
        this.children.push(tag)
    }
}