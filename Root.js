import Tag from "./model/Tag.js";

export default class Root {
    constructor() {

    }

    async start() {
        const main = document.getElementById('main')

        let _div1 = this.test_div_1()
        let _div2 = this.test_div_2()

        let div = this.build(_div1)
        main.appendChild(div)

        await this.sleep(2000)

        this.patch(div, _div2)
    }

    sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms))
    }

    build(vdom) {
        let current_element = document.createElement(vdom.tagName)
        if (vdom.classNames) {
            vdom.classNames.forEach(name => {
                current_element.classList.add(name)
            })
        }
        if (vdom.type) {
            current_element.setAttribute("type", vdom.type);
        }
        if (vdom.children) {
            vdom.children.forEach(child => {
                current_element.appendChild(this.build(child))
            })
        }
        if (vdom.id) {
            current_element.id = vdom.id
        }
        if (vdom.textNode) {
            vdom.textNode.forEach(node => {
                let text = String(node)
                if (text !== '[object Object]') {
                    current_element.appendChild(document.createTextNode(text))
                } else {
                    current_element.appendChild(this.build(node))
                }
            })
        }
        if (vdom.event) {
            current_element.addEventListener(vdom.event.type, vdom.event.callback, null)
        }
        return current_element
    }

    patch(dom, vdom) {
        if (dom.tagName !== vdom.tagName.toUpperCase()) {
            dom.replaceWith(this.build(vdom))
        } else {
            if (vdom.classNames) {
                vdom.classNames.forEach(name => {
                    if (!dom.classList.contains(name)) {
                        dom.classList.add(name)
                    }
                })
                dom.classList.forEach(name => {
                    if (!vdom.classNames.includes(name)) {
                        dom.classList.remove(name)
                    }
                })
            }
            if (vdom.children) {
                if (vdom.children.length === 0) {
                    while (dom.firstChild) {
                        dom.removeChild(dom.lastChild)
                    }
                } else if (dom.children.length < vdom.children.length) {
                    let i;
                    for (i = 0; i < dom.children.length; i++) {
                        this.patch(dom.children[i], vdom.children[i])
                    }
                    for (i; i < vdom.length; i++) {
                        dom.appendChild(this.build(vdom.children[i]))
                    }
                } else if (dom.children.length === vdom.children.length) {
                    for (let i = 0; i < dom.children.length; i++) {
                        this.patch(dom.children[i], vdom.children[i])
                    }
                } else {
                    while (dom.firstChild) {
                        dom.removeChild(dom.lastChild)
                    }
                    vdom.children.forEach(child => {
                        dom.appendChild(this.build(child))
                    })
                }
            }
            if (vdom.id) {
                if (dom.id !== vdom.id) {
                    dom.id = vdom.id
                }
            }
            if (vdom.textNode) {
                vdom.textNode.forEach(node => {
                    let text = String(node)
                    if (text !== '[object Object]') {
                        dom.appendChild(document.createTextNode(text))
                    } else {
                        dom.appendChild(this.build(node))
                    }
                })
            }
        }
    }

    test_div_1() {
        let p = new Tag()
        p.tagName = 'p'
        p.textNode = ['Text']
        p.classNames = ['test']

        let input = new Tag()
        input.tagName = 'input'
        input.type = 'text'

        let div = new Tag()
        div.tagName = 'div'
        div.addChildren(p)
        div.addChildren(input)

        return div
    }

    test_div_2() {
        let p = new Tag()
        p.tagName = 'p'
        p.textNode = ['Text modified']
        p.classNames = ['test']

        let input = new Tag()
        input.tagName = 'input'
        input.textNode = ['text']

        let div = new Tag()
        div.tagName = 'div'
        div.id = 'main-div'
        div.addChildren(p)
        div.addChildren(input)

        return div
    }
}