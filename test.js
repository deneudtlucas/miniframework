const main_content = document.getElementById('main-content')



async function main() {
}

main().then(r => {

})

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function buildVdom(vdom) {
    let current_element = document.createElement(vdom.tagName)
    if (vdom.classNames) {
        vdom.classNames.forEach(name => {
            current_element.classList.add(name)
        })
    }
    if (vdom.type) {
        current_element.setAttribute("type", vdom.type);
    }
    if (vdom.children) {
        vdom.children.forEach(child => {
            current_element.appendChild(buildVdom(child))
        })
    }
    if (vdom.id) {
        current_element.id = vdom.id
    }
    if (vdom.textNode) {
        vdom.textNode.forEach(node => {
            let text = String(node)
            if (text !== '[object Object]') {
                current_element.appendChild(document.createTextNode(text))
            } else {
                current_element.appendChild(buildVdom(node))
            }
        })
    }
    if (vdom.event) {
        current_element.addEventListener(vdom.event.type, vdom.event.callback, null)
    }
    return current_element
}

function patch(dom, vdom) {
    if (dom.tagName !== vdom.tagName.toUpperCase()) {
        dom.replaceWith(buildVdom(vdom))
    } else {
        if (vdom.classNames) {
            vdom.classNames.forEach(name => {
                if (!dom.classList.contains(name)) {
                    dom.classList.add(name)
                }
            })
            dom.classList.forEach(name => {
                if (!vdom.classNames.includes(name)) {
                    dom.classList.remove(name)
                }
            })
        }
        if (vdom.children) {
            if (vdom.children.length === 0) {
                while (dom.firstChild) {
                    dom.removeChild(dom.lastChild)
                }
            } else if (dom.children.length < vdom.children.length) {
                let i;
                for (i = 0; i < dom.children.length; i++) {
                    patch(dom.children[i], vdom.children[i])
                }
                for (i; i < vdom.length; i++) {
                    dom.appendChild(buildVdom(vdom.children[i]))
                }
            } else if (dom.children.length === vdom.children.length) {
                for (let i = 0; i < dom.children.length; i++) {
                    patch(dom.children[i], vdom.children[i])
                }
            } else {
                while (dom.firstChild) {
                    dom.removeChild(dom.lastChild)
                }
                vdom.children.forEach(child => {
                    dom.appendChild(buildVdom(child))
                })
            }
        }
        if (vdom.id) {
            if (dom.id !== vdom.id) {
                dom.id = vdom.id
            }
        }
        if (vdom.textNode) {
            dom.textContent = ''
            vdom.textNode.forEach(node => {
                let text = String(node)
                if (text !== '[object Object]') {
                    dom.appendChild(document.createTextNode(text))
                } else {
                    dom.appendChild(buildVdom(node))
                }
            })
        }
    }
}